# LogUpTs v6.x.x

A typesafe console replacement with formatter and transports inspired by
the python logging library. Its built with typescript and will run in the
browser and nodejs.

## all packages

For browsers a minimal filesize is essential so all formatters and
and transports are in seperated packages, so you only ship the
required code.

package|version|coverage
---|---|---
@logupts/transport-console|![coverage](https://gitlab.com/milleniumfrog/logupts/badges/master/coverage.svg?job=console_transport_coverage)|![coverage](https://img.shields.io/npm/v/@logupts/transport-console)
@logupts/core|![coverage](https://gitlab.com/milleniumfrog/logupts/badges/master/coverage.svg?job=core_coverage)|![coverage](https://img.shields.io/npm/v/@logupts/core)
@logupts/defaults|![coverage](https://gitlab.com/milleniumfrog/logupts/badges/master/coverage.svg?job=defaults_coverage)|![coverage](https://img.shields.io/npm/v/@logupts/defaults)
@logupts/transport-file|![coverage](https://gitlab.com/milleniumfrog/logupts/badges/master/coverage.svg?job=file_transport_coverage)|![coverage](https://img.shields.io/npm/v/@logupts/transport-file)
@logupts/formatter-preandpostfix|![coverage](https://gitlab.com/milleniumfrog/logupts/badges/master/coverage.svg?job=formatter_preandpostfix_coverage)|![coverage](https://img.shields.io/npm/v/@logupts/formatter-preandpostfix)
@logupts/transport-http|![coverage](https://gitlab.com/milleniumfrog/logupts/badges/master/coverage.svg?job=http_transport_coverage)|![coverage](https://img.shields.io/npm/v/@logupts/transport-http)
logupts|![coverage](https://gitlab.com/milleniumfrog/logupts/badges/master/coverage.svg?job=logupts_coverage)|![coverage](https://img.shields.io/npm/v/logupts)


## Console functions:

> After the beta I want to support all standardized console functions [see mdn for more information](https://developer.mozilla.org/en-US/docs/Web/API/Console)

- debug
- error
- info
- log
- warn


## Formatters

formatter| package | how it works
--- | --- | ---
joinFormatter | @logupts/defaults |  ['hello', 'world'] -> 'hello world' 
referenceFormatter | @logupts/defaults| ['hello', 'world'] -> ['hello', 'world']
copyFormatter | @logupts/defaults | ['hello', 'world'] -> [...['hello', 'world']]
prefix_postfixFormatter | @logupts/formatter-preandpostfix | ['hello', 'world'] -> prefix + 'hello world' + postfix

## Transports
transport | package | short description
--- | --- | ---
console | @logupts/transport-console | write your logdata to the browser/node console
file | @logupts/transport-file | write your logdata to a file.

## Versioning
All packages get managed by lerna so they support semantic versioning.

## Problems/Good Ideas/etc.

Please just create an issue or write an email to [to this mail](mailto:incoming+milleniumfrog-logupts-14412499-issue-@incoming.gitlab.com).