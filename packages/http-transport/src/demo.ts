import { HTTPTransport } from './index';

const transport = new HTTPTransport('http://localhost:9200/logupts/create');

transport.log({
  formatted: {'@timestamp': new Date().toISOString(), 'message': 'new message'},
  loglevel: 1,
  msg: [],
  name: 'log'
})