import { ITransport, ITransportArgs, IFormatter } from '@logupts/core';
import axios from 'axios';

export class HTTPTransport<M, Body> implements ITransport<M, Body> {

  constructor(public url: string, public optFormatter?: IFormatter<M, Body>) {}

  log(args: ITransportArgs<M, Body>) {
    const body = this.optFormatter ? this.optFormatter(args) : args.formatted;
    axios.post(this.url, body);
  }

  debug = this.log;
  warn = this.log;
  error = this.log;
  info = this.log;

}