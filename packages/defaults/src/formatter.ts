import { ITransportArgs, IFormatterArgs } from '@logupts/core';

export function joinFormatter<M>(args: IFormatterArgs<M>): ITransportArgs<M, string> {
  return {
    ...args,
    formatted: args.msg.join(' '),
  }
}

export function referenceFormatter<M>(args: IFormatterArgs<M>):  ITransportArgs<M, M[]> {
  return {
    ...args,
    formatted: args.msg
  }
}

export function copyFormatter<M>(args: IFormatterArgs<M>):  ITransportArgs<M, M[]> {
  return {
    ...args,
    formatted: [...args.msg]
  }
}