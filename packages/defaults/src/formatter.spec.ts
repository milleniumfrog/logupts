import { IFormatterArgs } from '@logupts/core';
import { joinFormatter, referenceFormatter, copyFormatter } from './formatter';

const args: IFormatterArgs<string> = {
  loglevel: 3,
  msg: ['hello', 'world'],
  name: 'log',
}

it('joinFormatter', () => {
  expect(joinFormatter(args).formatted).toBe('hello world');
})

it('referenceFormatter', () => {
  expect(referenceFormatter(args).formatted).toBe(args.msg);
})

it('copyformatter', () => {
  expect(copyFormatter(args).formatted).not.toBe(args.msg);
  expect(copyFormatter(args).formatted).toEqual(args.msg);
})