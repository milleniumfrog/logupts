import { ITransport, ITransportArgs, Loglevel } from '@logupts/core';
import fs from 'fs';

export type FiletransportArgs = {
  loglevel?: Loglevel;
  que: WritingQue;
  file: string | (() => string);
};

export default class FileTransport implements ITransport<any, string> {

  constructor(public o: FiletransportArgs) {}


  _handler(level: Loglevel, args: ITransportArgs<any, string>) {
    if (level <= (this.o.loglevel ?? args.loglevel)) {
      // print formatted message to console
      this.o.que.append(args.formatted);
    }
  }

  debug(args: ITransportArgs<any, string>) {
    this._handler(4 ,args);
  }

  /**
   * print formatted message to the console
   */
  error(args: ITransportArgs<any, string>) {
    this._handler(1 ,args);
  }

  info(args: ITransportArgs<any, string>) {
    this._handler(3 ,args);
  }

  log(args: ITransportArgs<any, string>) {
    this._handler(3 ,args);
  }

  warn(args: ITransportArgs<any, string>) {
    this._handler(2 ,args);
  }
}

export class WritingQue {

  _messages: string[];
  _active: boolean;
  constructor(private filePath: string, msgs: string[] = []) {
    this._messages = msgs;
    this._active = false;
    // start writing process if messages are passed in constructor
    if (this._messages.length > 0) {
      this._write();
    }
  }
  /** write messages from que in file */
  _write() {
    if (this._messages.length > 0) {
      this._active = true;
      fs.appendFile(this.filePath, this._messages.shift(), (err) => {
        if (err)
          throw err;
        this._write();
      })
    }
    else {
      this._active = false;
    }
  }

  public append(message: string) {
    this._messages.push(message);
    // start messages to file if not already done
    if (!this._active)
      this._write();
  }
}