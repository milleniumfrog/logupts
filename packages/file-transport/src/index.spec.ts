jest.mock('fs');

import { ITransportArgs } from '@logupts/core';
import fs from 'fs';
import FileTransport, { WritingQue } from './index';


it('append to file', () => {
  const transport = new FileTransport({file: './h.log', que: new WritingQue('./h.log')});
  const args: ITransportArgs<any, string> = {
    formatted: 'hello world',
    loglevel: 4,
    msg: [],
    name: 'log'
  }
  transport.log(args);
  transport.debug({...args, name: 'debug'});
  transport.error({...args, name: 'error'});
  transport.info({...args, name: 'info'});
  transport.warn({...args, name: 'warn'});
  expect(fs.appendFile).toHaveBeenCalledTimes(5);
  // expect error
  expect(() => transport.info({...args, name: 'info'})).toThrowError();
});

it('append to file with loglevel', () => {
  const transport = new FileTransport({file: './h.log', que: new WritingQue('./h.log'), loglevel: 1});
  const args: ITransportArgs<any, string> = {
    formatted: 'hello world',
    loglevel: 4,
    msg: [],
    name: 'log'
  }
  transport.log(args);
});

it('test que', () => {
  const que = new WritingQue('./s', ['hello world']);
  // set true to simulate that que is not finished because of writing to file
  que._active = true;
  que.append('abc');
});
