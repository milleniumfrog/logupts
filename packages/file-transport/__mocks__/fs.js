const fs = jest.genMockFromModule('fs');

const customAppendFile = jest.fn().mockImplementation((filepath, data, cb) => {
  cb(customAppendFile.mock.calls.length === 6 ? new Error('err') : null);
})

fs.appendFile = customAppendFile;

let err = null;

module.exports = fs;