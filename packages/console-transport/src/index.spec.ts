const orgLog = console.log,
  orgInfo = console.info,
  orgWarn = console.warn,
  orgError = console.error,
  orgDebug = console.debug;

console.log = jest.fn();
console.info = jest.fn();
console.warn = jest.fn();
console.error = jest.fn();
console.debug = jest.fn();

afterAll(() => {
  console.log = orgLog;
  console.info = orgInfo;
  console.warn = orgWarn;
  console.error = orgError;
  console.debug = orgDebug;
})


import ConsoleTransport from './index';
import { ITransportArgs } from '@logupts/core';

const demoTransportArgs: ITransportArgs<string, string> = {
  formatted: 'formatted',
  loglevel: 3,
  msg: ['msg'],
  name: 'log',
};

it('use default settings', () => {
  const transport = new ConsoleTransport();
  transport.debug({...demoTransportArgs, name: 'debug'});
  transport.error({...demoTransportArgs, name: 'error'});
  transport.info({...demoTransportArgs, name: 'info'});
  transport.log({...demoTransportArgs, name: 'log'});
  transport.warn({...demoTransportArgs, name: 'warn'});
})

it('modify settings', () => {
  const transportWithLoglevel = new ConsoleTransport({loglevel: 5});
  const transportWithLoglevelZero = new ConsoleTransport({loglevel: 0});
  transportWithLoglevel.log({...demoTransportArgs, name: 'log'})
  transportWithLoglevelZero.log({...demoTransportArgs, name: 'log'})
})