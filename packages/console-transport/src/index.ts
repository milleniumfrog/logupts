import { ITransport, Loglevel, ITransportArgs } from '@logupts/core';


export type ConsoleTransportArgs = {
  loglevel: Loglevel;
};

export default class ConsoleTransport implements ITransport<any, any> {

  constructor(public o?: ConsoleTransportArgs) {}

  _handler(level: Loglevel, args: ITransportArgs<any, any>) {
    if (level <= (this.o?.loglevel ?? args.loglevel))
      // print formatted message to console
      console[args.name](args.formatted);
  }

  debug(args: ITransportArgs<any, any>) {
    this._handler(4 ,args);
  }
  error(args: ITransportArgs<any, any>) {
    this._handler(1 ,args);
  }

  info(args: ITransportArgs<any, any>) {
    this._handler(3 ,args);
  }

  log(args: ITransportArgs<any, any>) {
    this._handler(3 ,args);
  }

  warn(args: ITransportArgs<any, any>) {
    this._handler(2 ,args);
  }
}