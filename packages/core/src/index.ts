/** Loglevels
 * 0 = no outputs at all |
 * 1 = errors |
 * 2 = warn |
 * 3 = info and log |
 * 4 = debug |
 * 5 = trace
 */
export type Loglevel = 0 | 1 | 2 | 3 | 4 | 5;
/** allowed functionsnames */
export type FunctionName = 'debug' | 'error' | 'info' | 'log' | 'warn';
export interface IFormatter<MsgType, FormattedType> {
  (args: IFormatterArgs<MsgType>): ITransportArgs<MsgType, FormattedType>;
}

export interface ITransport<MsgType, FormattedType> {
  debug: (args: ITransportArgs<MsgType, FormattedType>) => void;
  error: (args: ITransportArgs<MsgType, FormattedType>) => void;
  info: (args: ITransportArgs<MsgType, FormattedType>) => void;
  log: (args: ITransportArgs<MsgType, FormattedType>) => void;
  warn: (args: ITransportArgs<MsgType, FormattedType>) => void;
}

/** The formatter gets the following arguments */
export interface IFormatterArgs<MsgType> {
  /** called by which function (log, warn, info) */
  name: FunctionName
  msg: MsgType[];
  loglevel: Loglevel;
}

/** Transports get the following Arguments */
export interface ITransportArgs<MsgType, FormattedType> extends IFormatterArgs<MsgType> {
  formatted: FormattedType
}

export interface ILogUpTsOptions<MsgType, FormattedType> {
  /** The formatter converts the messagearray into an other type or format */
  formatter: IFormatter<MsgType, FormattedType>;
  /** output the logs to the following targets, e.g. the console */
  transports: ITransport<MsgType, FormattedType>[];
}

export class LogUpTs<M, F> implements ILogUpTsOptions<M, F> {

  formatter: IFormatter<M, F>;
  transports: ITransport<M, F>[];

  constructor(options: ILogUpTsOptions<M,F>) {
    this.formatter = options.formatter;
    this.transports = options.transports;
  }

  /** "private" function to perform the log functions (log, warn, error, info, warn, debug) */
  _handler({name, loglevel}: {name: FunctionName, loglevel: Loglevel}, ...msg: M[]): F {
    // get formatted messages
    const transportArgs = this.formatter({msg, name, loglevel});
    // handle all transports
    for (const transport of this.transports) {
      transport[name](transportArgs);
    }
    return transportArgs.formatted;
  }

  debug(...msg: M[]): F {
    return this._handler({name: 'debug', loglevel: 4}, ...msg);
  }
  
  error(...msg: M[]): F {
    return this._handler({name: 'error', loglevel: 1}, ...msg);
  }

  info(...msg: M[]): F {
    return this._handler({name: 'info', loglevel: 3}, ...msg);
  }

  log(...msg: M[]): F {
    return this._handler({name: 'log', loglevel: 3}, ...msg);
  }

  warn(...msg: M[]): F {
    return this._handler({name: 'warn', loglevel: 2}, ...msg);
  }
}