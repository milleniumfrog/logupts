import { LogUpTs, IFormatter, ITransport, FunctionName, ITransportArgs } from './index';

// define helper function
const formatterOne: IFormatter<string, string> = (args) => {
  return {
    ...args,
    formatted: `1-${args.loglevel}-${args.name} ${args.msg.join(' ')}`
  }
}
const formatterTwo: IFormatter<string, string> = (args) => {
  return {
    ...args,
    formatted: `2-${args.loglevel}-${args.name} ${args.msg.join(' ')}`
  }
}

const memorytransport: () => ITransport<string, string> & {outputs: Map<FunctionName, string[]>, args: ITransportArgs<string, string>[]} = () => {
  const outputs = new Map<FunctionName, string[]>([
    ['debug', []],
    ['error', []],
    ['info', []],
    ['log', []],
    ['warn', []]
  ]);
  const argsArr: ITransportArgs<string, string>[] = [];
  return {
    args: argsArr,
    outputs,
    debug(args) {
      outputs.get('debug')?.push(args.formatted);
      argsArr.push(args);
    },
    error(args) {
      outputs.get('error')?.push(args.formatted);
      argsArr.push(args);
    },
    info(args) {
      outputs.get('info')?.push(args.formatted);
      argsArr.push(args);
    },
    log(args) {
      outputs.get('log')?.push(args.formatted);
      argsArr.push(args);
    },
    warn(args) {
      outputs.get('warn')?.push(args.formatted);
      argsArr.push(args);
    }
  }
};

it('formatters', () => {
  // create loggers
  const logger = new LogUpTs({
    formatter: formatterOne,
    transports: []
  });
  const loggerTwo = new LogUpTs({
    formatter: formatterTwo,
    transports: []
  });
  // test output logger 1
  expect(logger.log('hello', 'world')).toBe('1-3-log hello world');
  // test output logger 2
  expect(loggerTwo.log('hello', 'world')).toBe('2-3-log hello world');
});

it('transports', () => {
  const transportOne = memorytransport();
  const transportTwo = memorytransport();
  const logger = new LogUpTs({formatter: formatterOne, transports: [transportOne, transportTwo]});
  logger.log('hello', 'world');
  logger.log('hello world');
  logger.warn('hello world');
  expect(transportOne.outputs.get('log')?.length).toBe(2);
  expect(transportOne.args[0]).toEqual({name: 'log', loglevel: 3, msg: ['hello', 'world'], formatted: '1-3-log hello world'});
  expect(transportTwo.args[0]).toEqual(transportOne.args[0]);
});

it('log', () => {
  const logger = new LogUpTs({
    formatter: formatterOne,
    transports: []
  });
  expect(logger.log('hello world')).toBe('1-3-log hello world');
})

it('error', () => {
  const logger = new LogUpTs({
    formatter: formatterOne,
    transports: []
  });
  expect(logger.error('hello world')).toBe('1-1-error hello world');
})

it('info', () => {
  const logger = new LogUpTs({
    formatter: formatterOne,
    transports: []
  });
  expect(logger.info('hello world')).toBe('1-3-info hello world');
})

it('warn', () => {
  const logger = new LogUpTs({
    formatter: formatterOne,
    transports: []
  });
  expect(logger.warn('hello world')).toBe('1-2-warn hello world');
})

it('debug', () => {
  const logger = new LogUpTs({
    formatter: formatterOne,
    transports: []
  });
  expect(logger.debug('hello world')).toBe('1-4-debug hello world');
})