##  (2020-04-07)

* feat: add express example ([0317f41](https://gitlab.com/milleniumfrog/logupts/commit/0317f41))
* feat: add http transport ([600f807](https://gitlab.com/milleniumfrog/logupts/commit/600f807))
* feat(transport): upgrade http transport ([89ae35d](https://gitlab.com/milleniumfrog/logupts/commit/89ae35d))
* chore: prepare examples ([3f3c718](https://gitlab.com/milleniumfrog/logupts/commit/3f3c718))
* chore: update LICENSE: 2019 -> 2020 ([47dacdc](https://gitlab.com/milleniumfrog/logupts/commit/47dacdc))
* docs: update READMEs ([a967ac4](https://gitlab.com/milleniumfrog/logupts/commit/a967ac4))
* docs(core): add more js docs ([d89693c](https://gitlab.com/milleniumfrog/logupts/commit/d89693c))
* ci: add core coverage job ([7895386](https://gitlab.com/milleniumfrog/logupts/commit/7895386))
* ci: add correct cache paths ([28fcd7b](https://gitlab.com/milleniumfrog/logupts/commit/28fcd7b))
* ci: add other tests ([3bf1d59](https://gitlab.com/milleniumfrog/logupts/commit/3bf1d59))
* ci: cache .yarn ([ab9466b](https://gitlab.com/milleniumfrog/logupts/commit/ab9466b))
* ci: fix ci ([5f1fd06](https://gitlab.com/milleniumfrog/logupts/commit/5f1fd06))
* ci: fix paths ([324236e](https://gitlab.com/milleniumfrog/logupts/commit/324236e))
* ci: move .yarn into cache ([21998b2](https://gitlab.com/milleniumfrog/logupts/commit/21998b2))
* ci: remove parallel build ([82dde5a](https://gitlab.com/milleniumfrog/logupts/commit/82dde5a))
* ci: speedup  build ([4c39b9b](https://gitlab.com/milleniumfrog/logupts/commit/4c39b9b))
* ci: speedup build performance ([7161621](https://gitlab.com/milleniumfrog/logupts/commit/7161621))
* ci: update ci ([a7b6650](https://gitlab.com/milleniumfrog/logupts/commit/a7b6650))
* test: add empty test so yarn run test does not fail ([e9f7e41](https://gitlab.com/milleniumfrog/logupts/commit/e9f7e41))
* fix(logupts): fixes output dir ([8e696cd](https://gitlab.com/milleniumfrog/logupts/commit/8e696cd))



## 6.0.0-beta.1 (2020-02-27)

* v6.0.0-beta.1 ([34fc8f6](https://gitlab.com/milleniumfrog/logupts/commit/34fc8f6))
* chore: update packages ([ac6491d](https://gitlab.com/milleniumfrog/logupts/commit/ac6491d))



## 6.0.0-beta.0 (2020-02-27)

* v6.0.0-beta.0 ([a090ab2](https://gitlab.com/milleniumfrog/logupts/commit/a090ab2))
* ci: add ci ([d2f27a0](https://gitlab.com/milleniumfrog/logupts/commit/d2f27a0))
* chore: . ([0272e43](https://gitlab.com/milleniumfrog/logupts/commit/0272e43))
* chore: add clear command ([0006c4f](https://gitlab.com/milleniumfrog/logupts/commit/0006c4f))
* chore: add full package ([f2b0eb5](https://gitlab.com/milleniumfrog/logupts/commit/f2b0eb5))
* chore: add npm ignore files ([8baf4eb](https://gitlab.com/milleniumfrog/logupts/commit/8baf4eb))
* chore: add typescript as devdep ([3dbdefe](https://gitlab.com/milleniumfrog/logupts/commit/3dbdefe))
* chore: boost build speed ([1381309](https://gitlab.com/milleniumfrog/logupts/commit/1381309))
* chore: change module output ([5a9e2bb](https://gitlab.com/milleniumfrog/logupts/commit/5a9e2bb))
* chore: delete packages for new implementation ([2f60c1d](https://gitlab.com/milleniumfrog/logupts/commit/2f60c1d))
* chore: gets test to 100%; ([5134376](https://gitlab.com/milleniumfrog/logupts/commit/5134376))
* chore: new interface setup ([fd8e800](https://gitlab.com/milleniumfrog/logupts/commit/fd8e800))
* chore: prepare v6 and disbale gitlab cicd ([4d41732](https://gitlab.com/milleniumfrog/logupts/commit/4d41732))
* chore: rename full to logupts ([9dc925d](https://gitlab.com/milleniumfrog/logupts/commit/9dc925d))
* chore: reset for v6 ([b67bd7b](https://gitlab.com/milleniumfrog/logupts/commit/b67bd7b))
* chore: set correct version ([4730655](https://gitlab.com/milleniumfrog/logupts/commit/4730655))
* feat: add basic logging functions ([9fc2c2a](https://gitlab.com/milleniumfrog/logupts/commit/9fc2c2a))
* feat: add core package v6 ([e5f24f4](https://gitlab.com/milleniumfrog/logupts/commit/e5f24f4))
* feat: add defaults package ([925bbc0](https://gitlab.com/milleniumfrog/logupts/commit/925bbc0))
* feat: add file transport ([aef6ac2](https://gitlab.com/milleniumfrog/logupts/commit/aef6ac2))
* feat: add formatter package ([e1f5759](https://gitlab.com/milleniumfrog/logupts/commit/e1f5759))
* feat: add prefix&postfix  formatter ([05eb6bb](https://gitlab.com/milleniumfrog/logupts/commit/05eb6bb))
* feat: add transport package ([2526764](https://gitlab.com/milleniumfrog/logupts/commit/2526764))
* feat: prepare for beta release ([5c28fa0](https://gitlab.com/milleniumfrog/logupts/commit/5c28fa0))
* feat: reworks prefix and postfix formatter ([7f40ba0](https://gitlab.com/milleniumfrog/logupts/commit/7f40ba0))
* test: add and update tests ([9061340](https://gitlab.com/milleniumfrog/logupts/commit/9061340))
* test: add missing tests ([b8a5185](https://gitlab.com/milleniumfrog/logupts/commit/b8a5185))
* test: adds missing tests ([7aa4f63](https://gitlab.com/milleniumfrog/logupts/commit/7aa4f63))
* test: mocks console output ([b66b033](https://gitlab.com/milleniumfrog/logupts/commit/b66b033))
* test: update tests ([448b52d](https://gitlab.com/milleniumfrog/logupts/commit/448b52d))
* test: update transport tests ([a74e873](https://gitlab.com/milleniumfrog/logupts/commit/a74e873))
* refactor: Console Transport is now an default export ([a52a52b](https://gitlab.com/milleniumfrog/logupts/commit/a52a52b))
* refactor: redirect calls to internal function ([c34673d](https://gitlab.com/milleniumfrog/logupts/commit/c34673d))
* refactor: rename functions ([aa208a8](https://gitlab.com/milleniumfrog/logupts/commit/aa208a8))
* fix: fixes loglevels ([ac176e9](https://gitlab.com/milleniumfrog/logupts/commit/ac176e9))
* build: fix build ([935d19d](https://gitlab.com/milleniumfrog/logupts/commit/935d19d))
* build: now outputs maps for .d.ts files ([af2443f](https://gitlab.com/milleniumfrog/logupts/commit/af2443f))
* style: default export the formatter functions ([5282d14](https://gitlab.com/milleniumfrog/logupts/commit/5282d14))
* style: remove comments and empty lines ([3777d47](https://gitlab.com/milleniumfrog/logupts/commit/3777d47))
* style: use typescript v3.7 syntax ([edc3e10](https://gitlab.com/milleniumfrog/logupts/commit/edc3e10))



## 5.1.0-alpha.2 (2019-11-21)

* v5.1.0-alpha.2 ([ae901eb](https://gitlab.com/milleniumfrog/logupts/commit/ae901eb))
* feat: add sync package ([c6421fd](https://gitlab.com/milleniumfrog/logupts/commit/c6421fd))
* fix: add module path to support rollup ([57f7bfc](https://gitlab.com/milleniumfrog/logupts/commit/57f7bfc))



## 5.1.0-alpha.1 (2019-11-18)

* v5.1.0-alpha.1 ([7234569](https://gitlab.com/milleniumfrog/logupts/commit/7234569))
* fix: fix publishconfigs ([008bc68](https://gitlab.com/milleniumfrog/logupts/commit/008bc68))



## 5.1.0-alpha.0 (2019-11-18)

* v5.1.0-alpha.0 ([c6bbe91](https://gitlab.com/milleniumfrog/logupts/commit/c6bbe91))
* feat: add assert, clear, count, countReset, dir ([94bd328](https://gitlab.com/milleniumfrog/logupts/commit/94bd328))
* feat: add basic transports ([b4ef935](https://gitlab.com/milleniumfrog/logupts/commit/b4ef935))
* feat: add dirxml ([e1520e3](https://gitlab.com/milleniumfrog/logupts/commit/e1520e3))
* feat: fix gitlab ci, add file transport, fix small bugs ([ad72220](https://gitlab.com/milleniumfrog/logupts/commit/ad72220))
* feat: logupts new concept ([9fcba3e](https://gitlab.com/milleniumfrog/logupts/commit/9fcba3e))
* chore: add npmignore files ([2dd613e](https://gitlab.com/milleniumfrog/logupts/commit/2dd613e))
* chore: reset copy.sh ([d2c3435](https://gitlab.com/milleniumfrog/logupts/commit/d2c3435))
* chore: reset v 5 ([235eef6](https://gitlab.com/milleniumfrog/logupts/commit/235eef6))



## <small>5.0.5 (2019-09-20)</small>

* v5.0.5 ([296a8fb](https://gitlab.com/milleniumfrog/logupts/commit/296a8fb))
* chore: add package transport-file ([e5ea532](https://gitlab.com/milleniumfrog/logupts/commit/e5ea532))



## <small>5.0.4 (2019-09-20)</small>

* v5.0.4 ([0d86dda](https://gitlab.com/milleniumfrog/logupts/commit/0d86dda))
* chore: make packages public ([0c13e89](https://gitlab.com/milleniumfrog/logupts/commit/0c13e89))



## <small>5.0.3 (2019-09-20)</small>

* v5.0.3 ([8222a65](https://gitlab.com/milleniumfrog/logupts/commit/8222a65))
* feat: add loglevels ([6e1712b](https://gitlab.com/milleniumfrog/logupts/commit/6e1712b))
* feat: defaults now support loglevels ([cb23eed](https://gitlab.com/milleniumfrog/logupts/commit/cb23eed))
* feat: export loglevel from index file ([523658f](https://gitlab.com/milleniumfrog/logupts/commit/523658f))
* ci: install lerna ([f7ee54f](https://gitlab.com/milleniumfrog/logupts/commit/f7ee54f))



## <small>5.0.2 (2019-09-20)</small>

* v5.0.2 ([6c357fe](https://gitlab.com/milleniumfrog/logupts/commit/6c357fe))
* ci: update ci cd ([7a44561](https://gitlab.com/milleniumfrog/logupts/commit/7a44561))
* chore: add README ([a2b3514](https://gitlab.com/milleniumfrog/logupts/commit/a2b3514))
* chore: remove github related files ([5620b18](https://gitlab.com/milleniumfrog/logupts/commit/5620b18))
* chore: rename package @logupts/logupts ([c045d6f](https://gitlab.com/milleniumfrog/logupts/commit/c045d6f))
* build: resolve node dependencies in rollup ([067cefb](https://gitlab.com/milleniumfrog/logupts/commit/067cefb))
* feat: add  packages async, defaults, sync ([040223b](https://gitlab.com/milleniumfrog/logupts/commit/040223b))
* fix: package paths to main, types, module ([160bd6b](https://gitlab.com/milleniumfrog/logupts/commit/160bd6b))



## <small>5.0.1 (2019-09-20)</small>

* v5.0.1 ([e730adc](https://gitlab.com/milleniumfrog/logupts/commit/e730adc))
* ci: add ci ([6dcc362](https://gitlab.com/milleniumfrog/logupts/commit/6dcc362))
* ci: build only when tagged ([1dd3b93](https://gitlab.com/milleniumfrog/logupts/commit/1dd3b93))
* chore: bump version ([35741d8](https://gitlab.com/milleniumfrog/logupts/commit/35741d8))
* chore: reset repository ([b28385e](https://gitlab.com/milleniumfrog/logupts/commit/b28385e))
* build: add build pipeline ([a93eeaf](https://gitlab.com/milleniumfrog/logupts/commit/a93eeaf))
* feat: v5 initial commit ([ec2b59e](https://gitlab.com/milleniumfrog/logupts/commit/ec2b59e))



## <small>4.0.5 (2019-08-30)</small>

* 4.0.5 ([b5526c1](https://gitlab.com/milleniumfrog/logupts/commit/b5526c1))
* test: add log output tests ([76bc8a9](https://gitlab.com/milleniumfrog/logupts/commit/76bc8a9))
* test: add logupts tests and move helper func in scope ([dbcb3e5](https://gitlab.com/milleniumfrog/logupts/commit/dbcb3e5))
* test: add tests for utilities and consoletransport ([bd5f54a](https://gitlab.com/milleniumfrog/logupts/commit/bd5f54a))
* fix: fix overloads and types ([40b23a1](https://gitlab.com/milleniumfrog/logupts/commit/40b23a1))
* fix: logupts now supports functions from loguptssync ([7d3270e](https://gitlab.com/milleniumfrog/logupts/commit/7d3270e))
* style: . ([f17fac5](https://gitlab.com/milleniumfrog/logupts/commit/f17fac5))
* feat: formatter now can output error.messages ([df75000](https://gitlab.com/milleniumfrog/logupts/commit/df75000))
* chore: update typescript -> 3.6.2 ([fa3268f](https://gitlab.com/milleniumfrog/logupts/commit/fa3268f))



## <small>4.0.4 (2019-08-28)</small>

* 4.0.4 ([cdb88c7](https://gitlab.com/milleniumfrog/logupts/commit/cdb88c7))
* fix: custom now passes errors to transportplugins ([d84648c](https://gitlab.com/milleniumfrog/logupts/commit/d84648c))



## <small>4.0.3 (2019-08-28)</small>

* 4.0.3 ([856b151](https://gitlab.com/milleniumfrog/logupts/commit/856b151))



## <small>4.0.2 (2019-08-28)</small>

* 4.0.2 ([5374757](https://gitlab.com/milleniumfrog/logupts/commit/5374757))
* fix: errors now print as errors, not as warnings ([97386a0](https://gitlab.com/milleniumfrog/logupts/commit/97386a0))



## <small>4.0.1 (2019-08-28)</small>

* 4.0.1 ([ff31af6](https://gitlab.com/milleniumfrog/logupts/commit/ff31af6))
* chore: . ([12e6930](https://gitlab.com/milleniumfrog/logupts/commit/12e6930))
* chore: remove rollup-resolve ([90af841](https://gitlab.com/milleniumfrog/logupts/commit/90af841))
* chore: update LICENSE ([e115f38](https://gitlab.com/milleniumfrog/logupts/commit/e115f38))
* feat: reexport all interfaces and constants and classes ([5b5c04c](https://gitlab.com/milleniumfrog/logupts/commit/5b5c04c))



## 4.0.0 (2019-08-28)

* 4.0.0 ([8c29c33](https://gitlab.com/milleniumfrog/logupts/commit/8c29c33))
* style: split logupts file in transport, formatter, ... ([58aedc8](https://gitlab.com/milleniumfrog/logupts/commit/58aedc8))
* build: change back to es2015 modules in tsconfig ([628370a](https://gitlab.com/milleniumfrog/logupts/commit/628370a))
* chore: move httpbeattransport in own repository ([ef00379](https://gitlab.com/milleniumfrog/logupts/commit/ef00379))
* chore: prepare v4 with deleting alls old files ([46a83f5](https://gitlab.com/milleniumfrog/logupts/commit/46a83f5))
* test: add first tests for logupts sync ([fec1217](https://gitlab.com/milleniumfrog/logupts/commit/fec1217))
* feat(v4): add alpha fo version 4 ([8772842](https://gitlab.com/milleniumfrog/logupts/commit/8772842))



## <small>3.0.12 (2019-05-28)</small>

* 3.0.12 ([a22aa0f](https://gitlab.com/milleniumfrog/logupts/commit/a22aa0f))
* fix path for module loaders ([dea1a48](https://gitlab.com/milleniumfrog/logupts/commit/dea1a48))



## <small>3.0.11 (2019-05-09)</small>

* ++ ([4fa3757](https://gitlab.com/milleniumfrog/logupts/commit/4fa3757))
* 2.0.2 ([06336ef](https://gitlab.com/milleniumfrog/logupts/commit/06336ef))
* 3.0.1 ([2c0ebba](https://gitlab.com/milleniumfrog/logupts/commit/2c0ebba))
* 3.0.10 ([37c61c2](https://gitlab.com/milleniumfrog/logupts/commit/37c61c2))
* 3.0.11 ([f3c0aed](https://gitlab.com/milleniumfrog/logupts/commit/f3c0aed))
* 3.0.2 ([55eb577](https://gitlab.com/milleniumfrog/logupts/commit/55eb577))
* 3.0.3 ([0782c84](https://gitlab.com/milleniumfrog/logupts/commit/0782c84))
* 3.0.4 ([138933b](https://gitlab.com/milleniumfrog/logupts/commit/138933b))
* 3.0.5 ([0bea24a](https://gitlab.com/milleniumfrog/logupts/commit/0bea24a))
* 3.0.6 ([234839a](https://gitlab.com/milleniumfrog/logupts/commit/234839a))
* 3.0.7 ([803164e](https://gitlab.com/milleniumfrog/logupts/commit/803164e))
* 3.0.8 ([7231f86](https://gitlab.com/milleniumfrog/logupts/commit/7231f86))
* 3.0.9 ([086bda5](https://gitlab.com/milleniumfrog/logupts/commit/086bda5))
* add dist/dist to gitignore ([f8bd680](https://gitlab.com/milleniumfrog/logupts/commit/f8bd680))
* add experimental trace and debug ([020f445](https://gitlab.com/milleniumfrog/logupts/commit/020f445))
* add loglevel, info ([144631b](https://gitlab.com/milleniumfrog/logupts/commit/144631b))
* add save to file support ([5ae5381](https://gitlab.com/milleniumfrog/logupts/commit/5ae5381))
* add strplace via github ([4d455b9](https://gitlab.com/milleniumfrog/logupts/commit/4d455b9))
* add type for logupts.bundle.js ([818b708](https://gitlab.com/milleniumfrog/logupts/commit/818b708))
* add umd as dist destination and removed spec files from dist ([fd43081](https://gitlab.com/milleniumfrog/logupts/commit/fd43081))
* added new specs ([f57f77d](https://gitlab.com/milleniumfrog/logupts/commit/f57f77d))
* added tests ([256e389](https://gitlab.com/milleniumfrog/logupts/commit/256e389))
* apply new build and test structure ([a4677cd](https://gitlab.com/milleniumfrog/logupts/commit/a4677cd))
* build ([d4ca5a7](https://gitlab.com/milleniumfrog/logupts/commit/d4ca5a7))
* build ([1d3999c](https://gitlab.com/milleniumfrog/logupts/commit/1d3999c))
* build ([e3e1ec3](https://gitlab.com/milleniumfrog/logupts/commit/e3e1ec3))
* build ([c9e4da1](https://gitlab.com/milleniumfrog/logupts/commit/c9e4da1))
* build ([199d6a9](https://gitlab.com/milleniumfrog/logupts/commit/199d6a9))
* build configs ([fd6d4db](https://gitlab.com/milleniumfrog/logupts/commit/fd6d4db))
* chai expect and shorts constructor ([46ea813](https://gitlab.com/milleniumfrog/logupts/commit/46ea813))
* configs and build ([7cc432c](https://gitlab.com/milleniumfrog/logupts/commit/7cc432c))
* corrected main path ([4857e4e](https://gitlab.com/milleniumfrog/logupts/commit/4857e4e))
* Create PULL_REQUEST_TEMPLATE ([0c75bf0](https://gitlab.com/milleniumfrog/logupts/commit/0c75bf0))
* doc update ([32068a9](https://gitlab.com/milleniumfrog/logupts/commit/32068a9))
* fix correct paths to include script add npm ignore ([62888c2](https://gitlab.com/milleniumfrog/logupts/commit/62888c2))
* fixed tests ([c9226e0](https://gitlab.com/milleniumfrog/logupts/commit/c9226e0))
* include strplace as own package ([70976d9](https://gitlab.com/milleniumfrog/logupts/commit/70976d9))
* initial commit for v3 ([b071b10](https://gitlab.com/milleniumfrog/logupts/commit/b071b10))
* InternalLogUpTsOptions ([ab15e83](https://gitlab.com/milleniumfrog/logupts/commit/ab15e83))
* logupts basic setup ([5a04294](https://gitlab.com/milleniumfrog/logupts/commit/5a04294))
* logutps class ([a22b7c8](https://gitlab.com/milleniumfrog/logupts/commit/a22b7c8))
* moved build scripts into sh files ([123c9c7](https://gitlab.com/milleniumfrog/logupts/commit/123c9c7))
* no message ([8b028b2](https://gitlab.com/milleniumfrog/logupts/commit/8b028b2))
* node test update ([c86f48c](https://gitlab.com/milleniumfrog/logupts/commit/c86f48c))
* remove build before merge ([9a57da6](https://gitlab.com/milleniumfrog/logupts/commit/9a57da6))
* remove build files ([480f232](https://gitlab.com/milleniumfrog/logupts/commit/480f232))
* remove builds before merge ([d0e8670](https://gitlab.com/milleniumfrog/logupts/commit/d0e8670))
* remove karma runner config ([adce557](https://gitlab.com/milleniumfrog/logupts/commit/adce557))
* remove old german doc ([1b7c112](https://gitlab.com/milleniumfrog/logupts/commit/1b7c112))
* remove strplace update script and build script ([a048952](https://gitlab.com/milleniumfrog/logupts/commit/a048952))
* reset to beginning ([7a236fb](https://gitlab.com/milleniumfrog/logupts/commit/7a236fb))
* Revert "3.1.0" ([7e0b919](https://gitlab.com/milleniumfrog/logupts/commit/7e0b919))
* rollup creates sourcemap to es2015 file ([9450e23](https://gitlab.com/milleniumfrog/logupts/commit/9450e23))
* set internalstype from any to generic T ([2d24494](https://gitlab.com/milleniumfrog/logupts/commit/2d24494))
* set readme to version 3 ([c6c0456](https://gitlab.com/milleniumfrog/logupts/commit/c6c0456))
* set to plural ([badaf7e](https://gitlab.com/milleniumfrog/logupts/commit/badaf7e))
* setup tests and build ([9d72d1d](https://gitlab.com/milleniumfrog/logupts/commit/9d72d1d))
* start with strplace ([85e585f](https://gitlab.com/milleniumfrog/logupts/commit/85e585f))
* uglify-es and Readme ([12492c2](https://gitlab.com/milleniumfrog/logupts/commit/12492c2))
* update gitignore ([0d3e871](https://gitlab.com/milleniumfrog/logupts/commit/0d3e871))
* Update issue templates ([5bb227e](https://gitlab.com/milleniumfrog/logupts/commit/5bb227e))
* update readme, update packages ([0351451](https://gitlab.com/milleniumfrog/logupts/commit/0351451))
* updated mails for ideas and questions ([e354bcd](https://gitlab.com/milleniumfrog/logupts/commit/e354bcd))



## <small>2.0.1 (2018-05-01)</small>

* 2.0.0 ([8374456](https://gitlab.com/milleniumfrog/logupts/commit/8374456))
* 2.0.1 ([fc713c2](https://gitlab.com/milleniumfrog/logupts/commit/fc713c2))
* add dist ([dd41799](https://gitlab.com/milleniumfrog/logupts/commit/dd41799))
* Custom Executions ([4e76529](https://gitlab.com/milleniumfrog/logupts/commit/4e76529))
* dist ([b92fefb](https://gitlab.com/milleniumfrog/logupts/commit/b92fefb))
* dist d.ts ([5ee9f66](https://gitlab.com/milleniumfrog/logupts/commit/5ee9f66))
* fixed package.json ([530ffa6](https://gitlab.com/milleniumfrog/logupts/commit/530ffa6))
* fixed tests ([8f41432](https://gitlab.com/milleniumfrog/logupts/commit/8f41432))
* logupts and tests ([3fcff3e](https://gitlab.com/milleniumfrog/logupts/commit/3fcff3e))
* logupts file support ([3e6c1a6](https://gitlab.com/milleniumfrog/logupts/commit/3e6c1a6))
* praefix -> prefix ([6e100ce](https://gitlab.com/milleniumfrog/logupts/commit/6e100ce))
* protected -> public ([fd04148](https://gitlab.com/milleniumfrog/logupts/commit/fd04148))
* protected -> public for all ([3282601](https://gitlab.com/milleniumfrog/logupts/commit/3282601))
* readme.md: configuration ([1bbe18e](https://gitlab.com/milleniumfrog/logupts/commit/1bbe18e))
* removed transportoptions ([4860012](https://gitlab.com/milleniumfrog/logupts/commit/4860012))
* Revert "basic setup for logupts 2.0.0" ([3b62e89](https://gitlab.com/milleniumfrog/logupts/commit/3b62e89))
* setup version 2.0.0 ([25080ae](https://gitlab.com/milleniumfrog/logupts/commit/25080ae))
* started new doc ([7c74f85](https://gitlab.com/milleniumfrog/logupts/commit/7c74f85))
* update .gitignore ([ee6e052](https://gitlab.com/milleniumfrog/logupts/commit/ee6e052))
* updated description ([132007c](https://gitlab.com/milleniumfrog/logupts/commit/132007c))
* build: dist ([db4acb9](https://gitlab.com/milleniumfrog/logupts/commit/db4acb9))



## <small>1.0.12 (2018-04-12)</small>

* 1.0.12 ([f206e9f](https://gitlab.com/milleniumfrog/logupts/commit/f206e9f))
* fixes two Placeholder bug ([d491805](https://gitlab.com/milleniumfrog/logupts/commit/d491805))



## <small>1.0.11 (2018-04-07)</small>

* 1.0.11 ([53a7152](https://gitlab.com/milleniumfrog/logupts/commit/53a7152))
* version 1.0.10 build -> 1.0.11 ([4a6ace3](https://gitlab.com/milleniumfrog/logupts/commit/4a6ace3))



## <small>1.0.10 (2018-04-07)</small>

* 1.0.10 ([3cfd0a5](https://gitlab.com/milleniumfrog/logupts/commit/3cfd0a5))
* custom hotfix, now uses options ([ad5dde9](https://gitlab.com/milleniumfrog/logupts/commit/ad5dde9))



## <small>1.0.9 (2018-04-06)</small>

* 1.0.9 ([5fa4bcf](https://gitlab.com/milleniumfrog/logupts/commit/5fa4bcf))
* build es2015 via tsc ([e4d4ed5](https://gitlab.com/milleniumfrog/logupts/commit/e4d4ed5))
* test, log to console when browser ([7e8fdc3](https://gitlab.com/milleniumfrog/logupts/commit/7e8fdc3))



## <small>1.0.8 (2018-04-06)</small>

* ... ([48c7551](https://gitlab.com/milleniumfrog/logupts/commit/48c7551))
* 1.0.8 ([8782b26](https://gitlab.com/milleniumfrog/logupts/commit/8782b26))
* tomac ... ([6143266](https://gitlab.com/milleniumfrog/logupts/commit/6143266))
* update ([ea29415](https://gitlab.com/milleniumfrog/logupts/commit/ea29415))



## <small>1.0.7 (2018-04-02)</small>

* 1.0.7 ([87d769a](https://gitlab.com/milleniumfrog/logupts/commit/87d769a))
* hotfix ([5b6c3af](https://gitlab.com/milleniumfrog/logupts/commit/5b6c3af))
* hotfix ([9faa1f4](https://gitlab.com/milleniumfrog/logupts/commit/9faa1f4))
* README update ([e7c8734](https://gitlab.com/milleniumfrog/logupts/commit/e7c8734))
* typescript output, split Placeholders from mainfile ([a2ada9a](https://gitlab.com/milleniumfrog/logupts/commit/a2ada9a))



## <small>1.0.5 (2018-03-31)</small>

* 1.0.5 ([5ee807d](https://gitlab.com/milleniumfrog/logupts/commit/5ee807d))
* doc & cdn setup ([175bbd8](https://gitlab.com/milleniumfrog/logupts/commit/175bbd8))



## <small>1.0.4 (2018-03-31)</small>

* 1.0.4 ([140d935](https://gitlab.com/milleniumfrog/logupts/commit/140d935))
* custom logs with log instead of info ([d1ce872](https://gitlab.com/milleniumfrog/logupts/commit/d1ce872))



## <small>1.0.3 (2018-03-31)</small>

* 1.0.3 ([eeb0099](https://gitlab.com/milleniumfrog/logupts/commit/eeb0099))
* small umd fix ([2d8a8b7](https://gitlab.com/milleniumfrog/logupts/commit/2d8a8b7))
* split umd and es2015 modules ([854e9da](https://gitlab.com/milleniumfrog/logupts/commit/854e9da))



## <small>1.0.2 (2018-03-30)</small>

* 1.0.2 ([9cdee6e](https://gitlab.com/milleniumfrog/logupts/commit/9cdee6e))
* fixes for pathes and tests ([537790b](https://gitlab.com/milleniumfrog/logupts/commit/537790b))



## <small>1.0.1 (2018-03-30)</small>

* 1.0.1 ([a106deb](https://gitlab.com/milleniumfrog/logupts/commit/a106deb))
* Initial commit ([29a8f18](https://gitlab.com/milleniumfrog/logupts/commit/29a8f18))
* package.json fix: ([4b42019](https://gitlab.com/milleniumfrog/logupts/commit/4b42019))



