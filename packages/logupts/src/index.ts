import { LogUpTs as Base, ITransport, IFormatter } from '@logupts/core';
import { joinFormatter } from '@logupts/defaults';
import ConsoleTransport from '@logupts/transport-console';

export * from '@logupts/core';
export * from '@logupts/defaults';
export * from '@logupts/transport-console';

export interface IArgs<M> {
  transports: ITransport<M, string>[];
  optionalFormatter: IFormatter<M, string>;
}

export class LogUpTs<T> extends Base<T, string> {
  constructor({optionalFormatter, transports}: Partial<IArgs<T>> = {transports: [], optionalFormatter: joinFormatter}) {
    super({
      formatter: optionalFormatter ?? joinFormatter,
      transports: [
        new ConsoleTransport(),
        ...(transports ?? []) ,
      ]
    });
  }
}