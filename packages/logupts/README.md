# LogUpTs v6

A typesafe console replacement with formatter and transports inspired by
the python logging library. Its built with typescript and will run in the
browser and nodejs.

## What does logupts do?

LogUpTs helps you:

- to format your outputs with formatters
- use consistent loglevels in node.js and in the browser
- transport your logdata to multiple targets like the console, files, databases etc.
- do everything typesafe


## Installation and usage

### Typescript

install the package and import it 

```
$ yarn add logupts
```

```typescript
import { LogUpTs } from 'logupts';
const logger = new LogUpTs(); // create
// just replace console with logger
// console.log('hello world'); // console output: "hello world"
logger.log('hello world'); // console output: "hello world"
logger.log('hello', 'world'); // console output: "hello world"
```

The logupts package is a predefined LogUpTs class that has a default
formatter and console-transport. You can use another formatter.

```typescript
import anotherFormatter from 'another-formatter'
const logger = new LogUpTs({
  optionalFormatter: anotherFormatter
});
```

or use additional transports

```typescript
import FileTransport, { WritingQue } from '@logupts/transport-file'
const logger = new LogUpTs({
  transports: [
    new FileTransport({que: new WritingQue(), file: 'myfile.log'}),
    ...
  ]
});
```

## Problems/Good Ideas/etc.

Please just create an issue or write an email to [to this mail](mailto:incoming+milleniumfrog-logupts-14412499-issue-@incoming.gitlab.com).