import { IFormatterArgs, ITransportArgs } from '@logupts/core';
import ArrayFactory, { FactoryArgs as ArrayFactoryArgs } from './array';
import StringFactory, { FactoryArgs as StringFactoryArgs } from './string';
// reexport Stringfactory
export { StringFactory, StringFactoryArgs };
// reexport Arrayfactory
export { ArrayFactory, ArrayFactoryArgs };

export type FactoryArgs<M> = {
  seperator?: string;
  prefix?: string | ((args: IFormatterArgs<M>) => string);
  postfix?: string | ((args: IFormatterArgs<M>) => string);
};

function joinFormatterFacotry(seperator: string) {
  return function<M>(args: IFormatterArgs<M>): ITransportArgs<M, string> {
    return {...args, formatted: args.msg.join(seperator)}
  };  
}

export default function PrefixPostfix<M>({seperator = ' ', prefix, postfix}: FactoryArgs<M> = {}) {
  return StringFactory({format: joinFormatterFacotry(seperator), prefix, postfix});
}