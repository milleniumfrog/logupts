import { IFormatterArgs } from '@logupts/core';
import PrefixPostfix from './index';

it('test seperators', () => {
  const args: IFormatterArgs<string> = {
    loglevel: 4,
    msg: ['hello', 'world'],
    name: 'log',
  };
  // default seperator
  expect(PrefixPostfix()(args).formatted).toBe('hello world');
  // custom seperator
  expect(PrefixPostfix({seperator: ','})(args).formatted).toBe('hello,world');
});

it('use prefix and postfix', () => {
  const args: IFormatterArgs<string> = {
    loglevel: 4,
    msg: ['hello', 'world'],
    name: 'log',
  };
  expect(PrefixPostfix({prefix: '[PRE] '})(args).formatted).toBe('[PRE] hello world');
  expect(PrefixPostfix({postfix: ' [POST]'})(args).formatted).toBe('hello world [POST]');
})