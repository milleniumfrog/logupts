import { IFormatter, IFormatterArgs } from '@logupts/core';

export type FactoryArgs<M, T> = {
  format: IFormatter<M, T[]>;
  prefix?: T | ((args: IFormatterArgs<M>) => T);
  postfix?: T | ((args: IFormatterArgs<M>) => T)
}

export default function<M, T>(init: FactoryArgs<M, T>): IFormatter<M, T[]> {
  return function(args: IFormatterArgs<M>) {
    // create tranArgs with default formatted content
    const tranArgs = init.format(args);
    if (init.prefix) {
      tranArgs.formatted.unshift(init.prefix instanceof Function ? init.prefix(args) : init.prefix);
    }
    if (init.postfix) {
      tranArgs.formatted.push(init.postfix instanceof Function ? init.postfix(args) : init.postfix);
    }
    return tranArgs;
  }
}