import { IFormatter, IFormatterArgs } from '@logupts/core';

export type FactoryArgs<M> = {
  format: IFormatter<M, string>;
  prefix?: string | ((args: IFormatterArgs<M>) => string);
  postfix?: string | ((args: IFormatterArgs<M>) => string);
}

export default function<M>(init: FactoryArgs<M>): IFormatter<M, string> {
  return function(args: IFormatterArgs<M>) {
    // create tranArgs with default formatted content
    const tranArgs = init.format(args);
    if (init.prefix) {
      tranArgs.formatted = (init.prefix instanceof Function ? init.prefix(args) : init.prefix) + tranArgs.formatted;
    }
    if (init.postfix) {
      tranArgs.formatted = tranArgs.formatted + (init.postfix instanceof Function ? init.postfix(args) : init.postfix);
    }
    return tranArgs;
  }
}