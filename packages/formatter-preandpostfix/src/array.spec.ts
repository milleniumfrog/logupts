import { IFormatterArgs } from '@logupts/core';
import PrefixPostfix from './array';

it('no prefix no postfix', () => {
  const args: IFormatterArgs<string> = {
    loglevel: 4,
    msg: ['hello', 'world'],
    name: 'log',
  };
  const forwardingFormatter = (args: IFormatterArgs<string>) => ({...args, formatted: [...args.msg]});
  const formatter = PrefixPostfix({format: forwardingFormatter});
  const newTranArgs = formatter(args);
  expect(JSON.stringify(newTranArgs.formatted)).toBe(JSON.stringify(args.msg));
  expect(newTranArgs.loglevel).toBe(args.loglevel);
  expect(newTranArgs.name).toBe(args.name);
});

it('with prefix', () => {
  const args: IFormatterArgs<string> = {
    loglevel: 4,
    msg: ['hello', 'world'],
    name: 'log',
  };
  const forwardingFormatter = (args: IFormatterArgs<string>) => ({...args, formatted:[...args.msg]});
  
  // when prefix is string
  const prefixString = '[PRE]';
  const formatterWithString = PrefixPostfix({format: forwardingFormatter, prefix: prefixString});
  const newTranArgsWithString = formatterWithString(args);
  expect(newTranArgsWithString.formatted[0]).toBe('[PRE]');
  expect(newTranArgsWithString.loglevel).toBe(args.loglevel);
  expect(newTranArgsWithString.name).toBe(args.name);

  // when prefix is function
  const prefixFunction = (args: IFormatterArgs<string>) => args.name;
  const formatterWithFunction = PrefixPostfix({format: forwardingFormatter, prefix: prefixFunction});
  const newTranArgsWithFunction = formatterWithFunction(args);
  expect(newTranArgsWithFunction.formatted[0]).toBe('log');
  expect(newTranArgsWithFunction.loglevel).toBe(args.loglevel);
  expect(newTranArgsWithFunction.name).toBe(args.name);
})

it('with postfix', () => {
  const args: IFormatterArgs<string> = {
    loglevel: 4,
    msg: ['hello', 'world'],
    name: 'log',
  };
  const forwardingFormatter = (args: IFormatterArgs<string>) => ({...args, formatted:[...args.msg]});
  
  // when prefix is string
  const postfixString = '[POSTFIX]';
  const formatterWithString = PrefixPostfix({format: forwardingFormatter, postfix: postfixString});
  const newTranArgsWithString = formatterWithString(args);
  expect(newTranArgsWithString.formatted[2]).toBe('[POSTFIX]');
  expect(newTranArgsWithString.loglevel).toBe(args.loglevel);
  expect(newTranArgsWithString.name).toBe(args.name);

  // when prefix is function
  const postfixFunction = (args: IFormatterArgs<string>) => args.name;
  const formatterWithFunction = PrefixPostfix({format: forwardingFormatter, postfix: postfixFunction});
  const newTranArgsWithFunction = formatterWithFunction(args);
  expect(newTranArgsWithFunction.formatted[2]).toBe('log');
  expect(newTranArgsWithFunction.loglevel).toBe(args.loglevel);
  expect(newTranArgsWithFunction.name).toBe(args.name);
})