import { IFormatterArgs } from '@logupts/core';
import PrefixPostfix from './string';

it('no prefix no postfix', () => {
  const args: IFormatterArgs<string> = {
    loglevel: 4,
    msg: ['hello', 'world'],
    name: 'log',
  };
  const joiningFormatter = (args: IFormatterArgs<string>) => ({...args, formatted: args.msg.join(' ')})
  const formatter = PrefixPostfix({format: joiningFormatter});
  const newTranArgs = formatter(args);
  expect(newTranArgs.formatted).toBe('hello world');
  expect(newTranArgs.loglevel).toBe(args.loglevel);
  expect(newTranArgs.name).toBe(args.name);
});

it('with prefix', () => {
  
  // prepare default args and a simple formatter
  const args: IFormatterArgs<string> = {
    loglevel: 4,
    msg: ['hello', 'world'],
    name: 'log',
  };
  const joiningFormatter = (args: IFormatterArgs<string>) => ({...args, formatted: args.msg.join(' ')})
  
  // when prefix was string it should be prefixed
  const stringPrefix = '[PRE] '
  const formatterStringPrefix = PrefixPostfix({format: joiningFormatter, prefix: stringPrefix});
  const newTranArgsWithString = formatterStringPrefix(args);
  expect(newTranArgsWithString.formatted).toBe('[PRE] hello world');
  expect(newTranArgsWithString.loglevel).toBe(args.loglevel);
  expect(newTranArgsWithString.name).toBe(args.name);
  // when prefix was function
  const functionPrefix = (args: IFormatterArgs<string>) => args.msg[0];
  const formatterFunctionPrefix = PrefixPostfix({format: joiningFormatter, prefix: functionPrefix});
  const newTranArgsWithFunction = formatterFunctionPrefix(args);
  expect(newTranArgsWithFunction.formatted).toBe('hellohello world');
  expect(newTranArgsWithFunction.loglevel).toBe(args.loglevel);
  expect(newTranArgsWithFunction.name).toBe(args.name);
})

it('with postfix', () => {
  
  // prepare default args and a simple formatter
  const args: IFormatterArgs<string> = {
    loglevel: 4,
    msg: ['hello', 'world'],
    name: 'log',
  };
  const joiningFormatter = (args: IFormatterArgs<string>) => ({...args, formatted: args.msg.join(' ')})
  
  // when postfix was string it should be postfixed
  const stringPostfix = ' [POST]'
  const formatterStringPostfix = PrefixPostfix({format: joiningFormatter, postfix: stringPostfix});
  const newTranArgsWithString = formatterStringPostfix(args);
  expect(newTranArgsWithString.formatted).toBe('hello world [POST]');
  expect(newTranArgsWithString.loglevel).toBe(args.loglevel);
  expect(newTranArgsWithString.name).toBe(args.name);
  // when postfix was function
  const functionpostfix = (args: IFormatterArgs<string>) => args.msg[0];
  const formatterFunctionpostfix = PrefixPostfix({format: joiningFormatter, postfix: functionpostfix});
  const newTranArgsWithFunction = formatterFunctionpostfix(args);
  expect(newTranArgsWithFunction.formatted).toBe('hello worldhello');
  expect(newTranArgsWithFunction.loglevel).toBe(args.loglevel);
  expect(newTranArgsWithFunction.name).toBe(args.name);
})