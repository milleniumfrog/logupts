import express, { Request } from 'express';
import { LogUpTs } from 'logupts';

const logger = new LogUpTs<{req: Request, time: number}>({
  optionalFormatter: (args) => {
    const request = args.msg[0];
    if (request) {
      const { method, path } = request.req;
      return {
        formatted: `[${method.toUpperCase()}] - ${path} - ${Date.now() - request.time} ms`,
        ...args,
      }
    }
    return {formatted: 'failed', ...args};
  }
});

const app = express();

app.use((req, res, next) => {
  const time = Date.now()
  res.on('finish', () => {
    logger.log({req, time});
  });
  res.on('close', () => {
    logger.log({req, time});
  });
  next();
})

app.get('/', (req, res) => {
  res.send('success');
});

app.listen(2000);
