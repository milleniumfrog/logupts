const terser = require('./node_modules/rollup-plugin-terser/index').terser;
const resolve = require('./node_modules/rollup-plugin-node-resolve/dist/rollup-plugin-node-resolve.cjs');

module.exports.rollupFactory =  function rollupFactory(fileNameWithoutExtension) {
    return {
        input: `dist/${fileNameWithoutExtension}.js`,
        output: {
            file: `dist/${fileNameWithoutExtension}.umd.js`,
            format: 'umd',
            name: `logupts${fileNameWithoutExtension === 'logupts' ? '' : `_${fileNameWithoutExtension}`}`,
            sourcemap: true,
        },
        plugins: [
            resolve(),
            terser({sourcemap: true}),
        ]
    }
}